/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam.enums;

/**
 *
 * @author peti
 */
public enum PlayerType {
    //The value empty makes sense only for cells not occupied by a palyer
    O, X, EMPTY;
}
